var hash = require('./hash').hash;
var users = {
	  demo: { name: 'demo' },
	  rentouch: { name: 'rentouch'}
	};

module.exports = function(){
	console.log("Start")
	for (var user in users) {
		console.log(user)
		hash('demo', function(err, salt, hash){
			if (err) throw err;
			users[user].salt = salt
			users[user].hash = hash
			console.log(users[user])
			console.log("salted user")
		})
	}
	console.log("END")
}