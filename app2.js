var express = require('express');
var bodyParser = require('body-parser'); // for reading POSTed form data into `req.body`
var expressSession = require('express-session');
var cookieParser = require('cookie-parser'); // the session is stored in a cookie, so we use this to parse it

var auth = require('./lib/auth')

var app = express();

// must use cookieParser before expressSession
app.use(cookieParser());
app.use(expressSession({secret:'somesecrettokenhere'}));
app.use(bodyParser());
app.use(bodyParser.json())

app.all('/*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type,X-Requested-With');
    next();
});


// Route
app.get('/', function(req, res){
	console.log(req.cookies)
  var html = '<form action="/" method="post">' +
             'Your name: <input type="text" name="userName"><br>' +
             '<button type="submit">Submit</button>' +
             '</form>';
  if (req.session.userName) {
    html += '<br>Your username from your session is: ' + req.session.userName;
  }
  res.send(html);
});

app.post('/', function(req, res){
  req.session.userName = req.body.userName;
  res.redirect('/');
});

app.get("/api/sessions", function(req, res){
    console.log("Current session-sessions:", req.session)
    res.json(req.session.userName)
    //return res.send(401, "Unauthorized")
    res.json([
    {
        id: 114,
        name: 'Session title',
        created: 1368836799,
        date: 0,
        owner: true,
        state: 0,
        favorite: true
    },
    {
        id: 10,
        name: 'Session title2',
        created: 1368836799,
        date: 0,
        owner: true,
        state: 0,
        favorite: true
    }
    ])
    res.end()
})

app.listen(3001);