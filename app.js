var express = require('express');
var bodyParser = require('body-parser')
var session = require('express-session')
var cookieParser = require('cookie-parser')

var auth = require('./lib/auth')

var app = express();

// *******************************
// middleware
app.use(cookieParser())
app.use(session({
    secret: 'dump-secret'
    //resave: false,
    //saveUninitialized: false
}))

app.use(bodyParser.json())

// Gloabl route (allow cross url)
app.all('/*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://127.0.0.1:4000');
    res.header('Access-Control-Allow-Headers', 'Content-Type, X-Requested-With');
    res.header('Access-Control-Allow-Credentials', true)
    next();
});


// *******************************
// routes
function restrict(req, res, next) {
    next();
}

app.get("/api/sessions", function(req, res){
    //console.log("Current session-sessions:", req.session)
    console.log("Got cookie:", req.cookies)
    //return res.send(401, "Unauthorized")
    res.end()
    console.log("Send cookie", res._headers["set-cookie"])
    /*res.json([
    {
        id: 114,
        name: 'Session title',
        created: 1368836799,
        date: 0,
        owner: true,
        state: 0,
        favorite: true
    },
    {
        id: 10,
        name: 'Session title2',
        created: 1368836799,
        date: 0,
        owner: true,
        state: 0,
        favorite: true
    }
    ])
    res.end()*/
})

app.post("/api/auth", function(req, res){
    //console.log("Current session-auth:", req.session)
    console.log("Got cookie (auth)", req.cookies)
    auth.authenticate(req.body.username, req.body.password, function(err, user){
        if (err) console.log(err)
        if (user){
            req.session.userName = "Hansli"
            res.end()
            console.log("Send cookie (auth)", res._headers["set-cookie"])
        }else{
            /*req.session.error = 'Authentication failed, please check your '
                + ' username and password.'
                + ' (use "tj" and "foobar")';
            res.redirect('/login');*/
            res.json(401, "Unauthorized")
        }
  })
})


// *******************************
// Start server
app.listen(3000);